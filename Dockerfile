FROM node:16
SHELL ["/bin/bash","-c"]
RUN apt-get install git \
&& git clone https://github.com/rbenv/rbenv.git ~/.rbenv \
&& cd ~/.rbenv && src/configure && make -C src \
&& export PATH="$HOME/.rbenv/bin:$PATH" \
&& echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc \
&& echo 'eval "$(rbenv init - bash)"' >> ~/.bashrc \
&& cat ~/.bashrc \
&& source ~/.bashrc \
&& echo $PATH \
&& mkdir -p "$(rbenv root)"/plugins \
&& git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build \
&& apt-get install -y libssl-dev \
&& rbenv install 3.1.2 \
&& rbenv shell 3.1.2 \
&& ruby -v 

WORKDIR /app

COPY server/package*.json ./

RUN npm install

COPY . .

WORKDIR /app/server 

RUN source ~/.bashrc && rbenv shell 3.1.2 && gem install pry && gem install sparql

EXPOSE 3000
CMD source ~/.bashrc && rbenv shell 3.1.2 && node index.js
