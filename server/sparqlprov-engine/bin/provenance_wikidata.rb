#!/usr/bin/env ruby
# frozen_string_literal: true

require 'pry'
require_relative '../sparqlprov/sparqlprov'
require_relative '../sparqlprov/decoding/polynomial_table'
require 'cgi'

unless ARGV.size > 0
  puts "usage:\provenance_wikidata.rb <query_file> [limit]" 
  exit 1
end

# Get the query to evaluate from a file in `ARGS[0]`.
input_query = File.new(ARGV[0]).read
# We set a limit in the number of answers to bring from the endpoint, 
limit = ARGV.size > 1 ? ARGV[1].to_i : 100

# Define the SPARQL endpoint
endpoint_url = 'https://query.wikidata.org/sparql'

# Define the reification scheme
reification_scheme = SPARQLprov.wikidata_reification

# Evaluate the query
evaluator = SPARQLprov::ProvenanceClientEvaluator.new(input_query,
                                                      endpoint_url,
                                                      reification_scheme,
                                                      limit)
answers = evaluator.answers
result = SPARQLprov::ProvenanceRelation.new(answers, 'prov')

# Print the result as JSON

# result["rewritten_query"] = evaluator.rewritten_sparql
puts "{ \"query\" :  \"#{CGI.escape(evaluator.rewritten_sparql)}\", \"solutions\": " + result.to_json + "}"
