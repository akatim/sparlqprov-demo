# frozen_string_literal: true

require 'sparql/client'

require_relative 'rewriting/rewriting'

module SPARQLprov
  # A client that sent and receives provenance aware queries.
  class ProvenanceClientEvaluator
    attr_reader :initial_query, :endpoint_url, :reification_scheme, :rewritten_sparql

    # Generates a new provenance client evaluator
    def initialize(initial_query, endpoint_url, reification_scheme, limit)
      @initial_query = initial_query
      @endpoint_url = endpoint_url
      @limit = limit
      @endpoint = SPARQL::Client.new(@endpoint_url)
      @rewriting = SPARQLprov::Rewriting.new(reification_scheme)
    end

    # Rewrite the initial query
    def rewrite
      @initial_exp = SPARQL.parse(@initial_query)
      @rewritten = @rewriting.rewrite_sxp(@initial_exp.to_sxp_bin, '?prov')
      @rewritten_sparql = SPARQLprov.sparql_query_encoding(@rewritten.query) + " LIMIT #{@limit}"
    end

    # Evaluate rewritten query
    def evaluate
      @answers = @endpoint.query(rewritten_query)
    end

    # Gets the initial query as a sxp
    def initial_sxp
      rewrite if @initial_exp.nil?
      @initial_exp.to_sxp
    end

    # Gets the rewritten query as a sxp
    def rewritten_sxp
      rewrite if @rewritten_exp.nil?
      @rewritten_exp.to_sxp
    end

    # Gets the rewritten query
    def rewritten_query
      rewrite if @rewritten_sparql.nil?
      @rewritten_sparql.to_s
    end

    # Gets the evaluation answers
    def answers
      evaluate if @answers.nil?
      @answers
    end
  end
end
