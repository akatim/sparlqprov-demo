# frozen_string_literal: true

require 'sparql'

# Implements SPARQLprov
module SPARQLprov
  # PREFIX = {
  #   wdt: 'http://www.wikidata.org/prop/direct/',
  #   p: 'http://www.wikidata.org/prop/',
  #   ps: 'http://www.wikidata.org/prop/statement'
  # }.freeze

  def self.test_query
    <<~QUERY
      PREFIX wdt: <http://www.wikidata.org/prop/direct/>
      PREFIX wd: <http://www.wikidata.org/entity/>
      SELECT ?person
      WHERE {
        ?person wdt:P31 wd:Q5 ;
                wdt:P166 wd:Q37922 ;
                wdt:P27 wd:Q298 .
      }
    QUERY
  end

  RewrittenQuery = Struct.new(:query, :sol_variables, :prov_variables)

  # Get the variables occurring in a Triple.
  #
  # This method assumes that triple is an array of the form:
  # [:triple, subject, predicate, object]
  def self.triple_variables(triple)
    Set.new(triple[1..].select { |value| value.is_a? RDF::Query::Variable })
  end

  # Apply the Wikidata reification to a triple
  #
  # This method assumes that triple is an array of the form:
  # [:triple, subject, predicate, object]
  def self.wikidata_reification
    lambda do |triple, statement_var|
      subject = triple[1]
      predicate = triple[2]
      object = triple[3]

      prefix_wdt = 'http://www.wikidata.org/prop/direct/'
      prefix_p = 'http://www.wikidata.org/prop/'
      prefix_ps = 'http://www.wikidata.org/prop/statement/'

      # This reification only applies to triples with the `wdt` prefix
      if /^#{prefix_wdt}/.match? predicate
        predicate_p = RDF::URI.new(predicate.to_s.sub(/^#{prefix_wdt}/, prefix_p))
        predicate_ps = RDF::URI.new(predicate.to_s.sub(/^#{prefix_wdt}/, prefix_ps))
        rewritten_triple = [:bgp,
                            [:triple, subject, predicate_p, statement_var],
                            [:triple, statement_var, predicate_ps, object]]
        RewrittenQuery.new(rewritten_triple, triple_variables(triple), Set.new([statement_var]))
      else
        RewrittenQuery.new(triple, triple_variables(triple), Set.new)
      end
    end
  end

  # Implements query rewriting
  class Rewriting
    def initialize(reification_scheme)
      @reification_scheme = reification_scheme
    end

    def rewrite(query_string)
      rewrite_sxp(SPARQL.parse(query_string).to_sxp_bin)
    end

    # Rewrite a SXP to compute how-provenance
    #
    # This method assumes that query is an array of the form [op, *]
    # where op is a symbol identifying the expression operation.
    def rewrite_sxp(query, prefix)
      case query[0]
      when :prefix
        rewrite_sxp(query[2], prefix)
      when :project
        rewrite_project(query[1], query[2], prefix)
      when :bgp
        rewrite_bgp(query, prefix)
      when :triple
        rewrite_triple(query, prefix)
      when :join
        rewrite_join(query[1], query[2], prefix)
      when :union
        rewrite_union(query[1], query[2], prefix)
      when :minus
        rewrite_minus(query[1], query[2], prefix)
      when :leftjoin
        rewrite_leftjoin(query[1], query[2], prefix)
      when :extend
        if query[2][0] == :group
          rewrite_extend_group(query[1], query[2][1], query[2][2], query[2][3], prefix)
        else
          rewrite_extend(query[1], query[2], prefix)
        end
      else
        raise "not implemented operator: #{query.inspect}"
      end
    end

    # The query has the form [:triple, list, query]
    def rewrite_triple(query, prefix)
      @reification_scheme.call(query, RDF::Query::Variable.new(prefix))
    end

    def extend_variables(extend_expression)
      extend_expression.map { |variable, _| variable }
    end

    def rewrite_extend(extend_expression, pattern, prefix)
      rewritten_pattern = rewrite_sxp(pattern, prefix)
      rewritten_query = [:extend, extend_expression, rewritten_pattern.query]
      sol_variables = Set.new(extend_variables(extend_expression))
                         .merge(rewritten_pattern.sol_variables)
      prov_variables = rewritten_pattern.prov_variables

      RewrittenQuery.new(rewritten_query, sol_variables, prov_variables)
    end

    def rewrite_extend_group(extend_expression, group_by, group_expression, pattern, prefix)
      rewritten_pattern = rewrite_sxp(pattern, "#{prefix}_aggregate_sum")

      sol_variables = Set.new
                         .merge(extend_expression.map { |var, _| var })
                         .merge(group_by)

      prov_aggregate = RDF::Query::Variable.new("#{prefix}_aggregate")
      prov_aggregate_sum = RDF::Query::Variable.new("#{prefix}_aggregate_sum")

      rewritten_query =
        [:extend,
         [mapping_binding(group_by, prov_aggregate),
          mapping_binding(group_by, prov_aggregate_sum)],
         [:join,
          [:project,
           group_by.sort + rewritten_pattern.prov_variables.to_a.sort,
           rewritten_pattern.query],
          [:extend,
           extend_expression,
           [:group, group_by, group_expression, pattern]]]]

      prov_variables = Set.new([prov_aggregate, prov_aggregate_sum])
                          .merge(rewritten_pattern.prov_variables)

      RewrittenQuery.new(rewritten_query, sol_variables, prov_variables)
    end

    # Rewrite a leftjoin operator
    #
    # Warning: This rewriting is based in an equivalence that does not
    # follows when pattern_one and pattern_two have disjoint sets of
    # strongly bound variables.
    def rewrite_leftjoin(pattern_one, pattern_two, prefix)
      equivalent_query =
        [:union,
         [:join, pattern_one, pattern_two],
         [:minus, pattern_one, pattern_two]]

      rewrite_sxp(equivalent_query, prefix)
    end

    def rewrite_join(pattern_one, pattern_two, prefix)
      rewritten_pattern_one = rewrite_sxp(pattern_one, "#{prefix}_sum_product_1")
      rewritten_pattern_two = rewrite_sxp(pattern_two, "#{prefix}_sum_product_2")
      sol_variables = Set.new(rewritten_pattern_one.sol_variables.to_a +
                              rewritten_pattern_two.sol_variables.to_a)
      prov_variables = Set.new(rewritten_pattern_one.prov_variables.to_a +
                               rewritten_pattern_two.prov_variables.to_a +
                               [RDF::Query::Variable.new("#{prefix}_sum"),
                                RDF::Query::Variable.new("#{prefix}_sum_product")])

      prov_sum = RDF::Query::Variable.new("#{prefix}_sum")
      prov_sum_product = RDF::Query::Variable.new("#{prefix}_sum_product")

      rewritten_query =
        [:extend,
         [mapping_binding(sol_variables, prov_sum),
          mapping_binding(sol_variables, prov_sum_product)],
         [:join,
          rewritten_pattern_one.query,
          rewritten_pattern_two.query]]

      RewrittenQuery.new(rewritten_query, sol_variables, prov_variables)
    end


    def rewrite_union(pattern_one, pattern_two, prefix)
      rewritten_pattern_one = rewrite_sxp(pattern_one, "#{prefix}_sum_osum_1")
      rewritten_pattern_two = rewrite_sxp(pattern_two, "#{prefix}_sum_osum_2")
      sol_variables = Set.new(rewritten_pattern_one.sol_variables.to_a +
                              rewritten_pattern_two.sol_variables.to_a)
      prov_variables = Set.new(rewritten_pattern_one.prov_variables.to_a +
                               rewritten_pattern_two.prov_variables.to_a +
                               [RDF::Query::Variable.new("#{prefix}_sum"),
                                RDF::Query::Variable.new("#{prefix}_sum_osum")])

      prov_sum = RDF::Query::Variable.new("#{prefix}_sum")
      prov_sum_osum = RDF::Query::Variable.new("#{prefix}_sum_osum")

      rewritten_query =
        [:extend,
         [mapping_binding(sol_variables, prov_sum),
          mapping_binding(sol_variables, prov_sum_osum)],
         [:union,
          rewritten_pattern_one.query,
          rewritten_pattern_two.query]]

      RewrittenQuery.new(rewritten_query, sol_variables, prov_variables)
    end

    def rewrite_minus(pattern_one, pattern_two, prefix)
      rewritten_pattern_one = rewrite_sxp(pattern_one, "#{prefix}_sum_difference_1")
      rewritten_pattern_two = rewrite_sxp(pattern_two, "#{prefix}_sum_difference_2_sum")
      sol_variables = Set.new(rewritten_pattern_one.sol_variables.to_a +
                              rewritten_pattern_two.sol_variables.to_a)
      prov_variables = Set.new(rewritten_pattern_one.prov_variables.to_a +
                               rewritten_pattern_two.prov_variables.to_a +
                               [RDF::Query::Variable.new("#{prefix}_sum"),
                                RDF::Query::Variable.new("#{prefix}_sum_difference"),
                                RDF::Query::Variable.new("#{prefix}_sum_difference_2_sum")])

      prov_sum = RDF::Query::Variable.new("#{prefix}_sum")
      prov_sum_difference = RDF::Query::Variable.new("#{prefix}_sum_difference")
      prov_sum_difference_2_sum = RDF::Query::Variable.new("#{prefix}_sum_difference_2_sum")

      rewritten_query =
        [:extend,
         [mapping_binding(sol_variables, prov_sum),
          mapping_binding(sol_variables, prov_sum_difference),
          mapping_binding(sol_variables, prov_sum_difference_2_sum)],
         [:leftjoin,
          rewritten_pattern_one.query,
          rewritten_pattern_two.query]]

      RewrittenQuery.new(rewritten_query, sol_variables, prov_variables)
    end

    # The query has the form [:bgp]
    def rewritten_empty_bgp(prefix)
      prov_variable = RDF::Query::Variable.new("#{prefix}_statement")
      rewritten_query = [:extend, [[prov_variable, RDF::Literal.new(1)]], [:bgp]]
      RewrittenQuery.new(rewritten_query, Set.new, Set.new([prov_variable]))
    end

    # The query has the form [:bgp,...]
    def rewrite_bgp(query, prefix)
      raise "Not a BGP: #{query.inspect}" unless query[0] == :bgp

      return rewritten_empty_bgp(prefix) if query.size == 1

      rewritten_bgp = [:bgp]
      sol_variables = Set.new
      prov_variables = Set.new
      statement_id = 1
      query[1..].each do |triple|
        statement_var = RDF::Query::Variable.new("#{prefix}_sum_product_#{statement_id}_statement")
        reified_statement = @reification_scheme.call(triple, statement_var)
        statement_id += 1 unless reified_statement.prov_variables.empty?
        prov_variables.merge reified_statement.prov_variables
        sol_variables.merge reified_statement.sol_variables
        case reified_statement.query[0]
        when :bgp
          rewritten_bgp += reified_statement.query[1..]
        when :triple
          rewritten_bgp << reified_statement.query
        end
      end

      rewritten_query = if prov_variables.empty?
                          prov_variable = RDF::Query::Variable.new("#{prefix}_statement")
                          prov_variables << prov_variable
                          [:extend, [[prov_variable, RDF::Literal.new(1)]], rewritten_bgp]
                        else
                          prov_sum = RDF::Query::Variable.new("#{prefix}_sum")
                          prov_sum_product = RDF::Query::Variable.new("#{prefix}_sum_product")
                          prov_variables << prov_sum
                          prov_variables << prov_sum_product
                          [:extend,
                           [mapping_binding(sol_variables.to_a.sort, prov_sum),
                            mapping_binding(sol_variables.to_a.sort, prov_sum_product)],
                           rewritten_bgp]
                        end

      RewrittenQuery.new(rewritten_query, sol_variables, prov_variables)
    end

    def rewrite_project(vars, subquery, prefix)
      rewritten_query = RewrittenQuery.new

      subprefix = "#{prefix}_sum"
      rewritten_subquery = rewrite_sxp(subquery, subprefix)
      projection_variable = RDF::Query::Variable.new("#{prefix}_sum")
      projected_vars = vars +
                       [projection_variable] +
                       rewritten_subquery.prov_variables.to_a
      rewritten_query.sol_variables = vars.to_set # TODO: Fix this to support aggregate queries
      rewritten_query.query = [:project,
                               projected_vars,
                               [:extend,
                               [mapping_binding(rewritten_query.sol_variables,
                                                projection_variable)],
                                rewritten_subquery.query]]
      rewritten_query.prov_variables =
        Set.new([projection_variable]).merge(rewritten_subquery.prov_variables)

      rewritten_query
    end

    def mapping_binding(mapping_variables, target_variable)
      vars = mapping_variables.map { |var| var.to_s.sub('?', '') }.sort

      vars_repr = vars.each_with_index.map do |var, index|
        sep = index.zero? ? '?' : '&'
        [RDF::Literal.new("#{sep}#{var}="),
         [:coalesce, [:sha1, RDF::Query::Variable.new(var)], RDF::Literal.new('')]]
      end

      [target_variable,
       [:iri, [:concat,
               RDF::Literal.new("http://example.org/#{target_variable.to_s.sub('?', '')}"),
               *vars_repr.reduce(:+)]]]
    end
  end

  def self.prefix_for_curi(curi)
    curi_pair = curi.to_sxp.split(':')
    prefix_name = "#{curi_pair[0]}:".to_sym
    prefix_value = RDF::URI.new curi.to_s.sub(/#{curi_pair[1]}$/, '')
    [prefix_name, prefix_value]
  end

  # Get the URIs occurring in an expression.
  def self.expression_uris(sxp)
    case sxp
    when Array
      if sxp[0] == :prefix
        expression_uris(sxp[2])
      else
        sxp[1..].map { |subsxp| expression_uris(subsxp) }.reduce([], :+)
      end
    when RDF::URI
      [sxp]
    else
      []
    end
  end

  # Get all prefixes in an expression
  def self.prefixes(sxp)
    expression_uris(sxp).reject { |uri| "<#{uri}>" == uri.to_sxp }.map do |uri|
      prefix_for_curi(uri)
    end.uniq
  end

  def self.sparql_query_encoding(sxp)
    sxp_prefixes = prefixes(sxp)
    sxp = [:prefix, sxp_prefixes, sxp] unless sxp.empty?

    SPARQL::Algebra.parse(sxp.to_sxp).to_sparql
    #SSEtoSPARQL.translate(sxp.to_sxp)
  end
end
