# SPARQLprov Demo

This is a web demo of the SPARQLprov algorithm that computes how-provenance annotations for the answers of the [Wikidata SPARQL endpoint](https://query.wikidata.org/). A running version of the demo can be found at [http://sparqlprov.cs.aau.dk/](http://sparqlprov.cs.aau.dk/)

## Software Requirements
- Nodejs (>=16)
- Ruby (3.1.2)

The demo consists of a web application, whose server backend is written in Nodejs. This backend integrates the SPARQLprov implementation and the communication with the Wikidata SPARQL endpoint. We use a Ruby [implementation of SPARQLprov](https://github.com/danielhz/sparqlprov-demo/tree/ruby_sxp_to_sparql) adapted for Web applications. For ease of deployment we have packed all the components in a Docker container. We explain how to deploy the container next. 

## Setting up the demo

### Download and Configuration 

The first step is to download the code of the demo. You can download it as a [zip file](https://gitlab.inria.fr/akatim/sparlqprov-demo/-/archive/main/sparlqprov-demo-main.zip) or clone it with Git, e.g., . 

```console
git clone https://gitlab.inria.fr/akatim/sparlqprov-demo
```

First, specify your server address in line 1 of the file client/app.js (inside the source code folder). By default the server address is the official's demo address:  

```js 
const ROOT_APP = "http://sparqlprov.cs.aau.dk"
```   
### Installing Docker

Then, we need to install Docker to build a container for the app with all the dependencies.  If you are using Debian or any of its derivatives, you can install Docker by running the following sequence of commands (more detailed instructions [here](https://docs.docker.com/engine/install/debian/)): 

```console
sudo apt-get install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

These commands set up the Docker package repository in your OS. To finally install Docker, run

```console
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

For other operating systems see [here](https://docs.docker.com/engine/install/).

### Deploying the demo

To build the Docker image from the Dockerfile, go inside the source code folder and run the following command:  

```console
docker build . -t sparqlprov-demo
```
Docker will collect all the demo dependencies in this image. This may take a while. Once finished, launch the container by running the image with the following command: 

```console
docker run -p 80:3000 -d sparqlprov-demo 
```
The demo should be deployed to the address you specified in ROOT_APP.

Note: Here we have assumed that the ROOT_APP variable points to a server listening in port 80 (the default HTTP port). If you want your demo to listen to a different port, e.g., 8080, this has to be reflected in ROOT_APP, e.g., "http://sparqlprov.cs.aau.dk:8080" and when launching the container: 

```console
docker run -p 8080:3000 -d sparqlprov-demo 
```
If you find any issues, do not hesitate to report them via our [issue tracker](https://gitlab.inria.fr/akatim/sparlqprov-internship/-/issues). 

### Extending the demo

If you want to add novel functionalities to the demo or fix some bugs, we recommend you not to use Docker because development can be tedious. In that case you can simply clone the repository and deploy it in your host OS, i.e., directly in your machine. To do so, you have first to make sure that your machine has all the software requirements installed (see section "Software Requirements" above). After that you can do as follows in your command line: 

```console
git clone https://gitlab.inria.fr/akatim/sparlqprov-demo
cd sparqlprov-demo/server
nodemon
```

The command ```nodemon``` will make your application available at http://localhost:3000, which you can access via a web browser. You could also run ```node index.js```, but contrary to this option, nodemon will reload the demo's server side application every time you make changes in the server code. This can be more convenient at development time.



## Roadmap

There are still a some room for improvement. Here is a list of the planned improvements. New feature requests are of course welcome.

- Reduce edge crossings in the graph by reimplementing the link distance functions in d3-force.
- Make the polynomial operators clickable

## Relevant Publications

- [Daniel Hernández](https://daniel.degu.cl/), [Luis Galárraga](http://luisgalarraga.de), [Katja Hose](https://homes.cs.aau.dk/~khose/). Computing How-Provenance for SPARQL Queries via Query Rewriting. Full paper at the Conference on Very Large Databases (VLDB), Sydney. [[Preprint]](http://luisgalarraga.de/docs/vldb-2022-provenance.pdf)
- [Luis Galárraga](http://luisgalarraga.de), [Daniel Hernández](https://daniel.degu.cl/), Anas Katim, [Katja Hose](https://homes.cs.aau.dk/~khose/). Visualizing How-Provenance Explanations for SPARQL Queries. Demo paper at The Web Conference, Austin. [[Full text]](https://dl.acm.org/doi/10.1145/3543873.3587350)
