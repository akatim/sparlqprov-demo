const SparqlClient = require('sparql-http-client');
const fs = require('fs');

let confRawData = fs.readFileSync('conf.json');
let conf = JSON.parse(confRawData);
const endpointUrl = conf["sparql-endpoint"];
const labelProp = conf["label-property"];
const labelService = conf["label-service"];
const tripleProp = conf["triple-property"];
const tripleSep = conf["triple-separator"];
const limitPerSt = conf["triples-limit-per-statement"]
const statement_template_path = './statement_template.txt';

/**
* It executes a given SPARQL construct query against the configured SPARQL
* endpoint declared in conf.json and writes the response in the given response object.
* The answer consists of a JSON object containing an array of triples (field 'triples) and 
* a set of mappings (field 'mappings') from RDF IRIs to string labels.
* @param {str} construct_query - The CONSTRUCT SPARQL query to be executed
* @param {http.ServerResponse} res - The response object that will be sent to the client 
*
*/
async function exec_construct_query(construct_query,res,label_pairs,std_prefixes) {
    const client = new SparqlClient({ endpointUrl});
    let quads = [];
    const triples = [];
    const mappings = {};
    const mappings_prefix = {};
    
    const statement_mappings = {};
    const seenTriples = new Set();
    const seenLabels = new Set();
    let bindings = await client.query.construct(construct_query,{ operation: 'postUrlencoded' });  
    // For some weird reason, this method is returning duplicates which are not accepted according
    // to the semantics of SPARQL CONSTRUCT  
    
    bindings.on('data', row => {
        Object.entries(row).forEach(([key, value]) => {
            const tripleStr = row["subject"].value + row["predicate"].value + row["object"].value;
            
            if (!seenTriples.has(tripleStr)) {
                seenTriples.add(tripleStr);
                        
                if (row["predicate"].value === labelProp)  {
                    mappings[row["subject"].value] = row["object"].value;
                    if (!seenLabels.has(row["object"].value)) {
                        seenLabels.add(row["object"].value);
                        mappings_prefix[row["object"].value] = format_labelProp(row["subject"].value,std_prefixes);
                    }
                } else {
                    if (row["predicate"].value === tripleProp) {
                        statement_mappings[row["object"].value] = row["subject"].value;
                    } else {
                        triples.push([row["subject"].value, row["predicate"].value, row["object"].value, ""]);
                    }
                }
            }
        })
    });
    bindings.on('end', () => {
        for (let triple of triples) {
            const tripleSt = triple[0] + tripleSep + triple[1] + tripleSep + triple[2];
            statementId = tripleSt in statement_mappings? statement_mappings[tripleSt] : "";
            const newTriple = [triple[0], triple[1], triple[2], statementId];
            quads.push(newTriple);
        }
        
        const responseObj = {"quads" : quads, "mappings": mappings, "mappings_prefix": mappings_prefix,...label_pairs};
        res.write(JSON.stringify(responseObj));
        res.end();
    });

    bindings.on('error', (error) => {
        console.log(error);
        const responseObj = {"error" : error}; 
        res.write(JSON.stringify(responseObj));
        res.end();
    });
}

/**
* It executes a given SPARQL select query against the configured SPARQL
* endpoint declared in conf.json and writes the response in the given response object.
* The answer consists of a JSON object containing an array of pairs (RDF IRIs - Labels).
* @param {str} select_query - The SELECT SPARQL query to be executed
*
*/
async function exec_select_query(select_query) {
    const client = new SparqlClient({ endpointUrl});
    const pairs = {};
    const responseObj = {};
    let bindings = await client.query.select(select_query,{ operation: 'postUrlencoded' });  

    bindings.on('data', row => {
        pairs[row.response.value]=row.responseLabel.value;
    });
    
    bindings.on('end', () => {
        responseObj["pairs"] = pairs;
    });

    bindings.on('error', (error) => {
        console.log(error);
    });

    return responseObj;
}

/**
* It builds and return CONSTRUCT SPARQL query with the union of each Wikidata statement given. 
* @param {Array} statements - Array of Wikidata statements
*/
function build_construct_query_unions(statements) {
    let construct_query = "";  let union_statements=""; let construct_statements = "";
    let query_template = fs.readFileSync(statement_template_path, 'utf8');  
    let count = 1;                                          

    statements.forEach((statement,index) => {
        construct_statements +=  `?subj${count} ?pr${count} ?obj${count} .\n` +
                                `?subj${count} ?p${count+1}l ?o${count+1} .\n` +
                                `?obj${count} ?p${count+2}l ?o${count+2} .\n` +
                                `?pr${count} rdfs:label ?pr${count}Label .\n` +
                                `?subj${count} rdfs:label ?subj${count}Label .\n` +
                                `?obj${count} rdfs:label ?obj${count}Label .\n` +
                                `?o${count+1} rdfs:label ?o${count+1}Label .\n` +
                                `?o${count+2} rdfs:label ?o${count+2}Label .\n` +
                                `?p${count+1}l rdfs:label ?p${count+1}Label .\n` +
                                `?p${count+2}l rdfs:label ?p${count+2}Label .\n` +
                                `?st${count} wds:triple ?st${count}_content .\n`;

        let bind_statement = `BIND(${statement} AS ?st${count})`;

        
        union_statements += query_template.replaceAll("<BIND_STATEMENT>",bind_statement)
                                                    .replaceAll("<C>",`${count}`)
                                                    .replaceAll("<C1>",`${count+1}`)
                                                    .replaceAll("<C2>",`${count+2}`)
                                                    .replace("<LIMIT_PER_ST>",limitPerSt);
        
        if (index !== 0) union_statements += '} \n';
        if (index !== statements.length-1) union_statements += 'UNION { \n';
        count++;
    });

    construct_query = `CONSTRUCT {\n ${construct_statements} \n} WHERE { \n` +
                        `SELECT * WHERE { ${union_statements} } }`;
                                                  
    console.log(construct_query);
                        
    return construct_query;   
}

/**
* It builds and return CONSTRUCT SPARQL query with the union of each Wikidata statement given. 
* @param {Array} other_answers - Array of URIs (answers)
*/
function build_select_query(other_answers) {
    let answers_values ="";

    other_answers.forEach(a => {
        answers_values += ` <${a}> \n `
    })

    let select_query = `SELECT * WHERE { \n` +
                        `VALUES ?response {${answers_values}} \n` +
                        `?response rdfs:label ?responseLabel \n`+
                        `filter(lang(?responseLabel) = 'en')\n}`;
    return select_query;
}

/**
* Function to format the label property according to the following structure : <prefix>:<localTerm>
* @param {str} full_labelProp - URI corresponding to the label property   
* @param {Object} prefixes - Set of prefixes (specified in the original query)
*/
function format_labelProp(full_labelProp,prefixes) {
    let labelProp="";
    let uri_prefix="";
    let labelProp_prefix="";

    //find prefix by taking the longest
    for (const prefix in prefixes) {
        if (full_labelProp.includes(prefixes[prefix]) && uri_prefix.length < prefixes[prefix].length) { 
            uri_prefix = prefixes[prefix]; 
            labelProp_prefix = prefix;
        }
    }

    labelProp = `${labelProp_prefix}:${full_labelProp.substring(uri_prefix.length)}`;
    
    return labelProp;
}

module.exports = {exec_construct_query, build_construct_query_unions,build_select_query,exec_select_query}